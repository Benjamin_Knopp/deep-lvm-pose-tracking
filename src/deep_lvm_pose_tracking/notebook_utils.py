import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
import torch
import numpy as np
from visdom import Visdom
import daft
from matplotlib import rc, animation
rc("font", family="serif", size=12)
rc("text", usetex=True)

from itertools import product
from functools import partial

from . import toy_data as toy

# useful for plotting on a 3x3 grid:
to_ind = np.array(list(product(range(3), range(3))))


def make_html_video(imgs):
    fig, ax = plt.subplots()
    img = imgs[0]
    mimg = plt.imshow(img)
    plt.close()

    def init():
        mimg.set_data(img)
        return (mimg,)

    def animate(i):
        img = imgs[i]
        mimg.set_data(img)
        return (mimg,)

    anim = animation.FuncAnimation(fig, animate, init_func=init,
                                   frames=len(imgs), interval=60,
                                   blit=True)
    return anim.to_html5_video()


def make_vae_graph():
    pgm = daft.PGM([3.2, 2.5], grid_unit=2)
    pgm.add_node(daft.Node("network", r"$\theta$", 1.0, 1.5, fixed=True))
    pgm.add_node(daft.Node("latent", r"$z_i$", 2.25, 2))
    pgm.add_node(daft.Node("image", r"$x_i$", 2.25, 1, observed=True))
    pgm.add_edge("latent", "image")
    pgm.add_edge("network", "image")
    pgm.add_plate(daft.Plate([1.6, 0.2, 1.2, 2.3], label=r"$N$"))
    return pgm

def make_kvae_graph():
    pgm = daft.PGM([5.2, 2.5], grid_unit=2)
    pgm.add_node(daft.Node("network", r"$\theta$", 3.75, 0.0, fixed=True))
    pgm.add_node(daft.Node("latent", r"$z_1$", 2.25, 2))
    pgm.add_node(daft.Node("latent2", r"$z_2$", 3.25, 2))
    pgm.add_node(daft.Node("latent3", r"$z_3$", 4.25, 2))
    pgm.add_node(daft.Node("latent4", r"$z_4$", 5.25, 2))
    pgm.add_node(daft.Node("image", r"$x_1$", 2.25, 1, observed=True))
    pgm.add_node(daft.Node("image2", r"$x_2$", 3.25, 1, observed=True))
    pgm.add_node(daft.Node("image3", r"$x_3$", 4.25, 1, observed=True))
    pgm.add_node(daft.Node("image4", r"$x_4$", 5.25, 1, observed=True))
    pgm.add_edge("latent", "image")
    pgm.add_edge("latent2", "image2")
    pgm.add_edge("latent3", "image3")
    pgm.add_edge("latent4", "image4")
    pgm.add_edge("network", "image")
    pgm.add_edge("network", "image2")
    pgm.add_edge("network", "image3")
    pgm.add_edge("network", "image4")
    pgm.add_edge('latent', 'latent2')
    pgm.add_edge('latent2', 'latent3')
    pgm.add_edge('latent3', 'latent4')
    # pgm.add_plate(daft.Plate([1.6, 0.2, 1.2, 2.3], label=r"$N$"))
    return pgm

def make_cvae_graph():
    pgm = daft.PGM([3.2, 2.5], grid_unit=2)
    pgm.add_node(daft.Node("network", r"$\theta$", 1.0, 1.5, fixed=True))
    pgm.add_node(daft.Node("latent", r"$z_i$", 2.25, 2))
    pgm.add_node(daft.Node("image", r"$x_i$", 1.9, 1, observed=False))
    pgm.add_node(daft.Node("pose", r"$c_i$", 2.6, 1, observed=True))
    pgm.add_edge("latent", "image")
    pgm.add_edge("network", "image")
    pgm.add_edge("pose", "latent", plot_params={'linestyle':'--'})
    pgm.add_edge("pose", "image")
    pgm.add_plate(daft.Plate([1.5, 0.3, 1.5, 2.3], label=r"$N$"))
    return pgm

class LatentTraverser(object):
    def __init__(self, model):
        self.model = model
        self.device = 'cpu'
        self.zeros = np.zeros((1, self.model.latent_dim))
        self.fig, self.ax = plt.subplots()
        self.d  = int(np.sqrt(self.model.input_dim))
        img = np.zeros((self.d, self.d))
        img[0, 0] = 1
        self.init_img = img
        self.mimg = plt.imshow(img)
        plt.close()

    def _init_img(self):
        self.mimg.set_data(self.init_img)
        return (self.mimg,)

    def _get_img(self, ind):
        zeros = np.zeros((1, self.model.latent_dim))
        for x in np.linspace(-4, 4):
            zeros[:, ind] = x
            mu_star = torch.Tensor(zeros).to(self.device)
            gen = self.model.decode(mu_star)[0]
            gen = gen.cpu().detach().numpy().reshape((self.d, self.d))
            # set_trace()
            yield (x, gen)

    def animate(self, img):
        x, img = img
        self.ax.set_title(f'{x:.3}')
        self.mimg.set_data(img)
        return (self.mimg,)

    def  get_anims(self, weight_dofs):
        anims = []
        for i in weight_dofs:
            generator = self._get_img(i)
            anim = FuncAnimation(self.fig, self.animate, init_func=self._init_img,
                                           frames=generator, interval=120,
                                           blit=True)
            anims += [anim]
        return anims


def make_bonelengths_and_width(n_bones=3, img_dim_sqrt=64):
    """
    Returns bone lengths and keymarker width with sensible defaults.
    """
    eps = np.random.rand(n_bones)
    bone_lengths = img_dim_sqrt//6 * (eps/2+1-1/n_bones)
    key_marker_width = 1.5 * img_dim_sqrt/32
    return bone_lengths, key_marker_width


def make_poses(N=36000):
    """
    Returns poses restricted such that there is no ambiguity.
    """
    poses = 1/2*np.pi*(np.random.rand(N, 3)-0.5)
    poses[:, 0] = poses[:, 0] * 4
    return poses


def pose_to_image(pose, bone_lengths, d):
    img = toy.keypoint_to_image(
        toy.forward(2*np.pi*(pose-0.5) , bone_lengths),
        size=(d, d),
        include_origin=True
    )
    return img

def un_normalize(poses):
    return 360*(poses-0.5)

def plot_reconstruction(recon, orig):
    fig, ax = plt.subplots(ncols=2)
    ax[0].imshow(recon)
    ax[1].imshow(orig)
    ax[0].set_title('Reconstruction of an validation image.')
    ax[1].set_title('Original')
    plt.show()

def plot_sample_grid(sample, img_shape):
    assert len(sample) >= 9
    fig, ax = plt.subplots(3, 3, sharex=True, sharey=True)
    fig.set_size_inches((10, 10))
    for i in range(9):
        img = sample[i]
        ind = to_ind[i]
        ax[ind[0], ind[1]].imshow(img.reshape(*img_shape))
    plt.tight_layout()
    plt.show()

def return_first(f):
    return lambda y: (f(y)[0])

def draw_samples(model, observed=None):
    model.to('cpu')
    if observed is None:
        decode = model.decode
    else:
        observed = torch.Tensor(observed)
        decode = partial(model.decode, observed=observed)

    decode = return_first(decode)

    with torch.no_grad():
        D_z = model.latent_dim
        sample = torch.randn(9, D_z)
        decoded = decode(sample)
        sample = decoded.numpy()
    return sample


class VisdomLinePlotter(object):
    """Plots to Visdom"""
    def __init__(self, env_name='vae_experiments'):
        self.viz = Visdom()
        self.env = env_name
        self.plots = {}
    def plot(self, var_name, split_name, title_name, x, y):
        if var_name not in self.plots:
            self.plots[var_name] = self.viz.line(X=np.array([x,x]),
                                                 Y=np.array([y,y]),
                                                 env=self.env,
                                                 opts=dict(
                                                     legend=[split_name],
                                                     title=title_name,
                                                     xlabel='Epochs',
                                                     ylabel=var_name)
                                                )
        else:
            self.viz.line(X=np.array([x]),
                          Y=np.array([y]),
                          env=self.env,
                          win=self.plots[var_name],
                          name=split_name,
                          update = 'append')

    def plot_image(self, var_name, inp):
        if type(inp) == np.ndarray:
            img = inp
        else:
            inp = inp[0, :4096].cpu().detach().numpy()
            img = inp.reshape(64, 64)
        if var_name not in self.plots:
            self.plots[var_name] = self.viz.heatmap(img)
        else:
            self.viz.heatmap(img, opts={'xmin':0, 'xmax':1},
                            env=self.env,
                            win=self.plots[var_name])
