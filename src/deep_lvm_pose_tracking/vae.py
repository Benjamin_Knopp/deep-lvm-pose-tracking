from tqdm import tqdm_notebook as tqdm

import torch
import torch.utils.data
from torch import nn, optim
from torch.nn import functional as F
from torch import Tensor as T
import torch.distributions as dist

import numpy as np

from contextlib import ExitStack
from math import pi
from functools import partial


def mean_var_activation(x):
    """
    Activation function for distributions using 2 parameters,
    with the second one constraint to (0, \infty).
    E.g. Normal: mu and sigma
    """
    N = x.shape[1]
    # return torch.sigmoid(x[:, :N//2]), torch.sigmoid(x[:, N//2:])
    return torch.sigmoid(x[:, :N//2]), torch.sigmoid(x[:, -1])


class InferenceNetwork(nn.Module):
    def __init__(self, input_dim, condition_dim, latent_dim, hidden, encoder_out_dim):
        nn.Module.__init__(self)
        self.conditioned = condition_dim > 0

        if self.conditioned:
            self.input = nn.Linear(input_dim, hidden//2)
            self.conditioner = nn.Linear(condition_dim, hidden//2)
        else:
            self.input = nn.Linear(input_dim, hidden)

        self.enc = nn.ModuleList([
            nn.Linear(hidden, encoder_out_dim)
        ])

        self.enc_activations = [
            # TODO: check encoder output activation in other implementations
            lambda x: x
        ]

    def forward(self, x, c):
        if self.conditioned:
            out = F.relu(torch.cat((self.input(x), self.conditioner(c)), dim=1))
        else:
            out = F.relu(self.input(x))

        for layer, activation in zip(self.enc, self.enc_activations):
            out = activation(layer(out))
        return out


class DecoderNetwork(nn.Module):
    pass


class cVAE(nn.Module):
    def __init__(self, input_dim, condition_dim, latent_dim=5, hidden=40,
                 likelihood='bernoulli',
                 condition_on='pose', full_covariance=True):
        nn.Module.__init__(self)
        self.likelihood = likelihood
        if likelihood in ['normal', 'cauchy', 'laplace']:
            dec_out_factor = 1
            out_activation = lambda x: (torch.sigmoid(x), None)
        else:
            dec_out_factor = 1
            # use dummy output for consistency
            out_activation = lambda x: (torch.sigmoid(x), None)

        self.full_covariance = full_covariance
        if self.full_covariance:
            encoder_out_dim = latent_dim + (latent_dim**2 + latent_dim)//2
        else:
            encoder_out_dim = 2 * latent_dim

        # Encoder
        self.encode = InferenceNetwork(input_dim, condition_dim,
                                       latent_dim, hidden, encoder_out_dim)

        # Decoder
        self.dec = nn.ModuleList([
            nn.Linear(latent_dim + condition_dim, hidden),
            nn.Linear(hidden, dec_out_factor * input_dim)
        ])
        self.dec_activations = [
            F.relu,
            out_activation
        ]

        self.latent_dim = latent_dim
        self.input_dim = input_dim
        self.condition_dim = condition_dim
        self.condition_on = condition_on
        self.image_dim = (input_dim
                          if condition_on == 'pose'
                          else condition_dim)

    def decode(self, z, observed):
        out = torch.cat((z, observed), dim=1)
        for layer, activation in zip(self.dec, self.dec_activations):
            out = activation(layer(out))
        return out

    def _get_pose_param(self, mu_obs, var_obs):
        pose_param = {}
        pose_param['mean'] = mu_obs[:, self.image_dim:]
        pose_param['var'] = (var_obs[:, self.image_dim:]
                              if self.likelihood != 'bernoulli' else None)
        return pose_param

    def forward(self, img, pose=None, anneal=1):
        img_param = {}

        M, D_x = img.shape
        device = img.device
        D_z = self.latent_dim

        # Use PCA weights instead of image if necessary
        x, c = ((img, pose)
                if self.condition_on == 'pose'
                else (pose, img))

        # Encode
        z_param = self.encode(x, c)
        if self.full_covariance:
            cov_tril = torch.zeros(M, D_z, D_z).to(device)
            tril_idx = torch.ones(D_z, D_z).tril() == 1
            cov_tril[:, tril_idx] = z_param[:, self.latent_dim:]
            z = dist.MultivariateNormal(loc=z_param[:, :D_z], scale_tril=cov_tril)
        else:
            ones = torch.eye(D_z, D_z).expand(M, D_z, D_z).to(device)
            cov_tril = ones.mm(z_param[:, D_z:, None])
            z = dist.MultivariateNormal(loc=z_param[:, :D_z], scale_tril=cov_tril)

        # Draw a sample and reparametrize: z = mu(x) + sigma * eps
        z_sampled = z.rsample()

        # Decode
        c = pose if self.condition_on == 'pose' else img

        # shape (:, input_dim+condition_dim), (:, input_dim+condition_dim)
        mu_x, var_x = self.decode(z_sampled, c)

        x = dist.Normal(mu_x, 0.2 * torch.ones(M, self.input_dim).to(device))

        return x, z


class VAE(cVAE):
    def __init__(self, input_dim, latent_dim=5, hidden=40,
                 likelihood='bernoulli'):
        cVAE.__init__(self, input_dim, latent_dim=latent_dim,
                      hidden=hidden, condition_dim=0,
                      likelihood=likelihood)
        self.input = nn.Linear(self.input_dim, hidden)

    def decode(self, z, observed=None):
        out = z
        for layer, activation in zip(self.dec, self.dec_activations):
            out = activation(layer(out))
        return out

    def _get_pose_param(self, mu_obs, var_obs):
        return None

    def forward_deprec(self, img, pose=None):
        mu, logvar = self.encode(img)
        z = reparameterize(mu, logvar)
        mu_x, var_x = self.decode(z)
        return mu_x, var_x, mu, logvar


def joint_loss(target, pose, x, z,
               likelihood='normal'):
    """
    Reconstruction + KL divergence losses summed over all elements
    and averaged over batch

    TODO: decide what to do with covariances x_param['var'].
    """
    M, D = target.shape
    device = target.device
    if likelihood == 'bernoulli':
        ## Binary cross entropy:
        # x_hat \log(x) + (1-x_hat) \log(1-x)
        # neg_llh_img = F.binary_cross_entropy(img_param['mean'],
        #                                      target, reduction='sum')
        x = dist.Bernoulli(probs=img_param['mean'])
        neg_llh_img = -torch.sum(x.log_prob(target))
    elif likelihood == 'normal':
        ## Mean squared error
        # + 0.5*M*D*np.log(0.5*pi)
        # neg_llh_img = torch.mean(
        #     (img_param['mean']-pre_param['latent'])**2  #/img_param['var']
        # )
        # x = dist.Normal(img_param['mean'], img_param['var'])
        neg_llh_img = -torch.mean(x.log_prob(target))
    else:
        raise(f'Observation model {likelihood} is not yet implemented')

    KLD = compute_kl_term(z)
    return neg_llh_img, KLD


def compute_kl_term(z):
    device = z.loc.device
    M, D_z = z.loc.shape
    prior_cov = torch.eye(D_z).expand(M, D_z, D_z).to(device)
    prior = dist.MultivariateNormal(torch.zeros(M, D_z).to(device),
                                    scale_tril=prior_cov)
    KLD = dist.kl.kl_divergence(z, prior)
    # excluding nans: these arise possibly due to bad initialization, with
    # negative values on the diagonal of z.scale_tril. But this problems
    # vanishes after few iterations.
    nan_idx = KLD != KLD
    KLD = torch.sum(KLD[~nan_idx])/torch.sum(~nan_idx)
    return KLD


def loss_function_deprec(decoded, x, mu, logvar, beta=1, likelihood='normal'):
    """
    decoded: reconstructed input (batch_size, dimension)
    x: input (batch_size, dimension)
    mu: \mu(x) - mean of q(z|x, \phi) (batch_size, latent_dim)
    logvar: \log(\sigma^2) of latent variable of input (batch_size, latent_dim)
    """
    img, pose  = x
    mu_img, var_img, mu_label, var_label = decoded
    if likelihood == 'bernoulli':
        rec_err = F.binary_cross_entropy(mu_img, img, reduction='sum')
        rec_err += F.mse_loss(mu_label, pose, reduction='sum')
    elif likelihood == 'normal':
        rec_err = 0.5 * torch.mean((mu_img - img)**2/var_img)
        rec_err += 0.5 * torch.mean((mu_label - pose)**2/var_label)
    elif likelihood == 'cauchy':
        x_0, gamma_sqr = decoded
        rec_err = -torch.log(gamma_sqr) + torch.log((x_0-x)**2+gamma_sqr)
        rec_err = torch.mean(rec_err)
    elif likelihood == 'laplace':
        mu_x, var_x = decoded
        rec_err = torch.mean(torch.abs(mu_x - x)/var_x + 2 * var_x)

    KLD = -0.5 * torch.sum(1 + logvar - mu.pow(2) - logvar.exp(), dim=1)
    KLD = torch.mean(KLD)
    return rec_err, beta * KLD


def fit(model, data_loader, epochs=3, verbose=True, optimizer=None,
        device='cpu', weight_fn=None, conditional=False,
        loss_func=None, plotter=None, beta=1, stop_crit=1e-4):
    """
    model: instance of nn.Module
    data_loader: Dictionary of pytorch.util.data.DataSet for training and
                 validation
    """
    if optimizer is None:
        optimizer = torch.optim.RMSprop(model.parameters(), lr=1e-3)
    # if loss_func is None:
    #     loss_func = partial(loss_function, beta=1, likelihood=model.likelihood)

    stop = False  # is set to True if stopping criteria is fullfilled

    phases = ['train', 'val']

    all_train_loss = []
    anneal = 0.0001
    for epoch in range(epochs):
        if stop:
            break

        for phase in phases:
            if phase == 'pretrain' and epoch > 0:
                continue
            epoch_loss = []
            kls = []
            N = len(data_loader[phase].dataset)
            M = data_loader[phase].batch_size

            # conditional context manager
            with ExitStack() as stack:
                if phase in ['pretrain', 'train']:
                    model.train()
                    pbar = stack.enter_context(tqdm(data_loader[phase]))
                else:
                    model.eval()
                    stack.enter_context(torch.no_grad())
                    pbar = stack.enter_context(tqdm(data_loader[phase], disable=True))

                batch_idx = 0  # tqdm does not like enumerate
                for batch in pbar:
                    batch_idx += 1

                    # get data
                    img = batch['image'].view(data_loader[phase].batch_size, -1)
                    pose = T(batch['angles'].float()).to(device) if conditional else None
                    img = T(img.float()).to(device)

                    img_param, z = model(img, pose)
                    target = pose if model.condition_on == 'image' else img
                    neg_llh_img, kl = joint_loss(target, pose, img_param, z,
                                                 likelihood='normal')

                    if phase == 'train':
                        loss = neg_llh_img + anneal * beta * kl
                    elif phase == 'val':
                        loss = neg_llh_img + anneal * kl
                    else:
                        raise(f'phase {phase} not known')

                    optimizer.zero_grad()
                    if phase in ['pretrain', 'train']:

                        with torch.autograd.detect_anomaly():
                            loss.backward()
                        prev_loss = loss.item()
                        if plotter is not None and batch_idx % 50 == 0:
                            visdom_plot(plotter, model, beta, epoch_loss,
                                        prev_loss, phase, neg_llh_img,
                                        kl,
                                        img_param, img)

                            e_np = np.array(epoch_loss)
                            if (len(e_np) > 2000 and
                                np.abs(np.diff(e_np[-1900:]).mean()) < stop_crit):
                                print('Loss stopped decreasing.')
                                stop = True
                                break

                        epoch_loss += [prev_loss]
                        kls += [kl.item()]
                        optimizer.step()
                    else:
                        epoch_loss += [loss.item()]
                    if verbose and phase == 'train':
                        pbar.set_description(
f"({phase}) Epoch {epoch}, Loss at batch {batch_idx:05d}: {loss.item()*M/N:.2E}"
                        )
            if phase == 'train':
                all_train_loss += epoch_loss
                epoch_loss = loss.item()
            else:
                epoch_loss = np.sum(np.array(epoch_loss))
            print(f'{phase} loss: {epoch_loss*M/N:.2E}')
            if weight_fn is not None:
                torch.save(model.state_dict(), weight_fn)
    # return all_train_loss
    return epoch_loss*M/N  # last validation loss for gpyopt


def visdom_plot(plotter, model, beta, epoch_loss,
                prev_loss, phase, neg_llh_img,
                kl,
                img_param, img):
    fmt = (beta, model.latent_dim)
    plotter.plot('Loss_{:.2f}_{}'.format(*fmt), 'Val', 'Loss',
                 len(epoch_loss), prev_loss)

    plotter.plot(f'img_llh_{beta:.2f}_{model.latent_dim}',
                 'Val', 'img_llh',
                 len(epoch_loss), neg_llh_img.item())


    plotter.plot('kl_{:.2f}_{}'.format(*fmt),
                 'Val', 'kl',
                 len(epoch_loss), kl.item())

    plotter.plot_image('original', img)

if __name__ == '__main__':
    cVAE(1, 1)
